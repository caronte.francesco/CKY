# coding: utf8

colTag = 3
wordCount = {}
wordTagCount = {}
tagCount = {}
tagTagCount = {}
train = []
nPhrase = 0
lowerWord = True
morph = {}
morphEn = False

def init(morph_aux,wl):
    load_train()
    load_hash_table()
    if bool(morph_aux):
        morph = morph_aux
        morphEn = True
    if wl:
        wordLower = True


def viterbi_aux(observations):
    #print colTag
    o = observations
    state_graph = getstategraph(observations)
    T = len(o)
    N = len(state_graph)

    viterbi = [[float(0) for i in range(0, T + 1)] for k in range(0, N + 2)]
    backpointer = [[None for i in range(0, T + 1)] for k in range(0, N + 2)]

    # init step
    for s in range(0, N):
        if state_graph[s][0] != None:
            viterbi[s + 1][1] = float(a("START", state_graph[s][0])) * float(
                b(state_graph[s][0], o[0]))  # state_graph indici corretti
            backpointer[s + 1][1] = [0, 0]

    # recursion step
    for t in range(2, T + 1):
        for s in range(0, N):
            list = []
            for s1 in range(0, N):
                if state_graph[s][t - 1] != None and state_graph[s1][t - 2] != None:
                    val = float(viterbi[s1 + 1][t - 1]) * float(
                        a(state_graph[s1][t - 2], state_graph[s][t - 1])) * float(b(state_graph[s][t - 1], o[t - 1]))
                    list.append([val, [s1 + 1, t - 1]])
            if len(list) > 0:
                viterbi[s + 1][t] = max(list)[0]
                backpointer[s + 1][t] = max(list)[1]

    # final step
    list = []
    for s1 in range(0, N):
        if state_graph[s1][T - 1] != None:
            val = float(viterbi[s1 + 1][T]) * float(a(state_graph[s1][T - 1], "END"))
            list.append([val, [s1 + 1, T]])
    if len(list) > 0:
        viterbi[0][T] = max(list)[0]
        backpointer[0][T] = max(list)[1]

    r = backpointer[0][T]
    res = []
    while r != None:
        x = r[0]
        y = r[1]
        if x-1<0 or y-1<0:
            break
        status = state_graph[x - 1][y - 1]
        if status != None:
            res.append(status)
            r = backpointer[x][y]
        else:
            r = None
    res.reverse()

    return res


def load_train():
    in_file = "it-universal-train.conll"
    with open(in_file) as f:
        line_train = f.readlines()

    for i in line_train:
        train.append(i.split('\t'))

def load_hash_table():
    #print "COLTAG: "+str(colTag)
    print "load hash table"
    tprec = 'START'
    for line in train:
        #print line
        if len(line) > 1:
            tag = line[colTag]
            load_tag(tag)
            word = line[1]
            if lowerWord:
                word = word.lower()
            if morphEn:
                word = get_morph(word)
            load_word(word)
            load_wordtag(word,tag)
            load_tag_tag(tprec,tag)
            tprec = tag
        else:
            load_tag_tag(tprec,"END")
            tprec = "START"



def load_tag_tag(t1,t2):
    key = t1+"/"+t2
    if key in tagTagCount:
        val = tagTagCount[key]
        tagTagCount[key] = val + 1.0
    else:
        tagTagCount[key] = 1.0

def load_tag(tag):
    if tag in tagCount:
        val = tagCount[tag]
        tagCount[tag] = val + 1.0
    else:
        tagCount[tag] = 1.0

def load_word(word):
    if word in wordCount:
        val = wordCount[word]
        wordCount[word] = val + 1.0
    else:
        wordCount[word] = 1.0

def load_wordtag(word,tag):
    key = word+"-"+tag
    if key in wordTagCount:
        val = wordTagCount[key]
        wordTagCount[key] = val + 1.0
    else:
        wordTagCount[key] = 1.0

def a(t, t1):  # probabilità di transazione
    return float(float(c(t, t1)) / float(cs(t)))


def b(t, w):  # probabilità di emissione
    if w not in wordCount:
        return 1.0/11.0

    val = float(float(ct(t, w)) / float(cs(t)))

    return val

def baseline(phrase):
    res = []
    for w in phrase:
        res.append(gettag(w))

    return res

def ct(t, w):  # conta il numero di volte in cui la parola w ha il tag T
    key = w+"-"+t
    if key in wordTagCount:
        return float(wordTagCount[key])
    else:
        return 0

def cs(t): #
    if t in tagCount:
        return float(tagCount[t])
    else:
        return 1.0/11.0

def c(t, t1):  # conta il numero di volte che t si trova prima di t1
    key = t+"/"+t1
    if key in tagTagCount:
        return tagTagCount[key]
    else:
        return 0.0

def getstategraph(phrase):
    hash = {}
    max = 0
    matrix = []
    bool = 0
    for t in phrase:
        listTemp = []
        for i in train:
            if len(i) > 1 and i[1].lower() == t.lower() and i[colTag] not in listTemp:
                listTemp.append(i[colTag])
                bool = 1
        if bool == 0:
            listTemp.append('NOUN')
        if len(listTemp) > max:
            max = len(listTemp)
        hash[t] = listTemp
        matrix.append(listTemp)
        bool = 0

    matrix = [[None for i in range(0, len(phrase))] for k in range(0, max)]
    c = 0
    for i in phrase:
        temp = hash[i]
        for k in range(0, len(temp)):
            matrix[k][c] = temp[k]
        c = c + 1

    return matrix


def viterbi(phrase):
    return viterbi_aux(phrase)

def decode(phrase):
    for i in range(0,len(phrase)):
        phrase[i] = str(phrase[i]).decode('utf8','strict')
    return phrase

def get_morph(word):
    word = word.lower()
    if word in morph:
        word = morph[word]
    return word

