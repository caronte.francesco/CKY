# coding=utf-8
import viterbi
import time

def compare(list1, list2):
    c = 0.0
    for i in range(0, len(list1)):
        add_tagsol(list2[i])
        if str(list1[i]) == str(list2[i]):
            c += 1.0
            add_tagres(list1[i])
    return c

def load_morph():
    in_file = "morph-it.txt"
    with open(in_file) as f:
        line_train = f.readlines()

    for i in line_train:
        line = i.split('\t')
        key = line[0]
        val = line[1]
        morph[key] = val

def get_morph(word):
    word = word.lower()
    if word in morph:
        word = morph[word]
    return word

def accSingleTag():
    res = []
    for t in tagTot:
        val = float(tag_res[t]/tag_sol[t])
        #print t +" & " +str(val) +"\\\\"
        print t +" & " +str(1.0-val)+"\\\\"
        #res.append([t,val])

def add_tagres(tag):
    global tag_res
    if tag in tag_res:
        val = tag_res[tag]
        tag_res[tag] = val + 1.0
    else:
        tag_res[tag] = 1.0

def add_tagsol(tag):
    global tag_sol
    if tag in tag_sol:
        val = tag_sol[tag]
        tag_sol[tag] = val + 1.0
    else:
        tagTot.append(tag)
        tag_sol[tag] = 1.0

if __name__ == '__main__':

    colTag = 4
    wordLower = False
    morphEn = False
    morph = {}
    global tag_res,tag_sol
    tag_sol = {}
    tag_res = {}
    tagTot = []
    flags =[[False, False],[True,False],[False,True]]

    for flag in flags:

        wordLower = flag[0]
        morphEn = flag[1]
        print "Word lower: "+str(wordLower)
        print "morphEn: "+str(morphEn)
        if morphEn:
            load_morph()

        viterbi.init(morph,wordLower,colTag)
        tot = 0
        in_file = "it-universal-test.conll"
        with open(in_file,'r') as f:
            line_test = f.readlines()
            tot = len(line_test)

        phrase = []
        result = []
        c = 0
        numWord = 0
        start = time.time()
        for i in range(0, tot):
            if 1 < len(line_test[i].split('\t')):
                word = line_test[i].split('\t')[1]
                if wordLower:
                    word = word.lower()
                if morphEn:
                    word = get_morph(word)
                phrase.append(word)
                result.append(line_test[i].split('\t')[colTag])
                numWord += 1.0
            else:
                resultViterbi = viterbi.viterbi(phrase)
                c += compare(resultViterbi,result)
                phrase = []
                result = []
        end = time.time()
        #print "p: "+str(p)
        print "c: "+ str(c)
        print "numWord: "+str(numWord)
        print "Accuratezza: " + str(float(c)/float(numWord) * 100.0) +"%"
        print "Time: " + str(abs(float(start - end)))
        accSingleTag()