train = []
wordCount = {}
wordTagCount = {}
tagCount = {}
#wordLower = False
#morphEn = False
morph = {}
global tag_res,tag_sol
tag_sol = {}
tag_res = {}


def load_train():
    print "Load training set"
    in_file = "it-universal-train.conll"
    with open(in_file) as f:
        line_train = f.readlines()

    for i in line_train:
        train.append(i.split('\t'))


def load_morph():
    in_file = "morph-it.txt"
    with open(in_file) as f:
        line_train = f.readlines()

    for i in line_train:
        line = i.split('\t')
        key = line[0]
        val = line[1]
        morph[key] = val


def load_hash_table():
    print "Load hash table"
    global tagTot
    tagTot = []
    for line in train:
        if len(line) > 1:
            tag = line[colTag]
            tag = line[colTag]
            load_tag(tag)
            word = line[1]
            if wordLower:
                word = word.lower()
            if morphEn:
                word = get_morph(word)
            load_word(word)
            load_wordtag(word, tag)
            tagTot.append(tag)
    tagTot = list(set(tagTot))


def load_tag(tag):
    if tag in tagCount:
        val = tagCount[tag]
        tagCount[tag] = val + 1.0
    else:
        tagCount[tag] = 1.0


def load_word(word):
    # word = word.lower()
    if word in wordCount:
        val = wordCount[word]
        wordCount[word] = val + 1.0
    else:
        wordCount[word] = 1.0


def load_wordtag(word, tag):
    # word = word.lower()
    key = word + "-" + tag
    # print key
    if key in wordTagCount:
        val = wordTagCount[key]
        wordTagCount[key] = val + 1.0
    else:
        wordTagCount[key] = 1.0


def b(t, w):  # probabilit di emissione
    val = float(ct(t, w) / cs(t))

    return val


def gettag(word):
    list = []
    if word not in wordCount:
        return 'NOUN'
    for t in tagTot:
        list.append([float(b(t, word)), t])
        # if word==debug:
        # print list
    return max(list)[1]


def baseline(phrase):
    res = []
    for w in phrase:
        res.append(gettag(w))
    return res


def ct(t, w):  # conta il numero di volte in cui la parola w ha il tag T
    key = w + "-" + t
    if key in wordTagCount:
        return float(wordTagCount[key])
    else:
        return 0


def cs(t):
    if t in tagCount:
        return float(tagCount[t])


def printError(phrase, list1, list2):
    for i in range(0, len(phrase)):
        if list1[i] != list2[i]:
            print phrase
            print "termine: " + str(phrase[i])
            print "tag corretto: " + str(list1[i])
            print "tag assegnato: " + str(list2[i])
            print "tag presenti nel dizionaro per la parola " + str(phrase[i])
            for t in tagTot:
                if phrase[i] + "-" + t in wordTagCount:
                    print t


def compare(list1, list2):
    c = 0.0
    for i in range(0, len(list1)):
        add_tagsol(list2[i])
        if str(list1[i]) == str(list2[i]):
            c += 1.0
            add_tagres(list1[i])
    return c


def get_morph(word):
    word = word.lower()
    if word in morph:
        word = morph[word]
    return word

def accSingleTag():
    res = []
    for t in tagTot:
        val = float(tag_res[t]/tag_sol[t])
        print "tag: "+t+" accuratezza: "+str(val)
        print "tag: "+t+" error: "+str(1.0-val)

        #res.append([t,val])

def add_tagres(tag):
    global tag_res
    if tag in tag_res:
        val = tag_res[tag]
        tag_res[tag] = val + 1.0
    else:
        tag_res[tag] = 1.0

def add_tagsol(tag):
    global tag_sol
    #print tag_sol
    if tag in tag_sol:
        val = tag_sol[tag]
        tag_sol[tag] = val + 1.0
    else:
        tag_sol[tag] = 1.0

#####main
'''
if __name__ == '__main__':

    colTag = 4
    flags =[[False, False],[True,False],[False,True]]

    for flag in flags:

        wordLower = flag[0]
        morphEn = flag[1]
        print "Word lower: "+str(wordLower)
        print "morphEn: "+str(morphEn)
        if morphEn:
            load_morph()

        load_train()
        load_hash_table()

        tot = 0
        in_file = "it-universal-test.conll"
        with open(in_file, 'r') as f:
            line_test = f.readlines()
            tot = len(line_test)


        phrase = []
        result = []
        c = 0
        numWord = 0
        for i in range(0, tot):
            if 1 < len(line_test[i].split('\t')):
                word = line_test[i].split('\t')[1]
                if wordLower:
                    word = word.lower()
                if morphEn:
                    word = get_morph(word)
                phrase.append(word)
                result.append(line_test[i].split('\t')[colTag])
                numWord += 1.0
            else:
                resultBaseline = baseline(phrase)
                c += compare(resultBaseline, result)
                phrase = []
                result = []

        print "c: " + str(c)
        print "numWord: " + str(numWord)
        print "Accuratezza: " + str(float(c) / float(numWord) * 100.0) + "%"
        accSingleTag()
'''
def init():
    global colTag, wordLower, morphEn
    colTag = 3
    wordLower = False
    morphEn = True
    load_morph()
    load_train()
    load_hash_table()

