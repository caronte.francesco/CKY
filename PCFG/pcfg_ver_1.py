from nltk.tree import Tree
import nltk
import random
import os
import sys

hashRule = {}
grammarWords = {}

# carica morph-it

def load_morph():
    morph = {}
    in_file = "morph-it.txt"
    with open(in_file) as f:
        line_train = f.readlines()

    for i in line_train:
        line = i.split('\t')
        key = line[0]
        val = line[1]
        morph[key] = val

    return morph

# assegna un indice ad ogni non-terminale
# e lo memorizza in una hash table

def generateNonTerminal(grammar):
    nonterminali = {}

    count = 0
    prod = sorted(grammar.productions())
    for rule in prod:
        A = str(rule.lhs())
        if A not in nonterminali.keys():
            nonterminali[A] = count
            count += 1

    return nonterminali


# carica il dataset, count indica il numero
# di frasi che faranno parte del testset

def load_dataset():
    test = []
    in_file = "tut-clean-simple.penn"
    prod = []
    with open(in_file) as f:
        line_train = f.readlines()
        #cont = 2
        cont = (len(line_train) * 10)/ 100
        for i in line_train:
            treeTemp = Tree.fromstring(i)
            phrase = getPhrase(morph(treeTemp))
            if len(phrase) < 20 and cont > 0 and random.randint(0,1) == 1:
                test.append([phrase,i])
                cont -= 1
            else:
                t = Tree.fromstring(i)
                t.collapse_unary(collapsePOS=True)
                t.chomsky_normal_form()
                t = morph(t)
                prod += t.productions()

    S = nltk.Nonterminal('S')
    grammar = nltk.induce_pcfg(S, prod)

    return grammar, test


# Costruisce la stringa

def buildTree(back, val):
    if val[0] == -1:
        return '(' + val[1] + ' ' + val[2] + ')'
    s = '(' + val[3] + ' ' + buildTree(back, back[val[0]][val[1]][val[4]]) + ' ' + buildTree(back, back[val[1]][val[2]][
        val[5]]) + ' )'
    return s

# Algoritmo principale

def probabilistcky(words, grammar, nonterminali):
    N = len(words) + 1
    table = [[[0.0 for a in nonterminali.keys()] for j in range(0, N)] for i in range(0, N)]
    back = [[[0.0 for a in nonterminali.keys()] for j in range(0, N)] for i in range(0, N)]

    roots = ['S', 'NP', 'PP', 'PRN']

    for j in range(1, N):
        for rule in sorted(grammar.productions(rhs=words[j - 1])):
            A = nonterminali[str(rule.lhs())]
            table[j - 1][j][A] = rule.prob()
            back[j - 1][j][A] = -1, str(rule.lhs()), words[j - 1]

        for i in reversed(range(-1, j - 1)):
            for k in range(i + 1, j + 1):
                for rule in grammar.productions():
                    if len(rule.rhs()) == 2:
                        prob = rule.prob()
                        bc = rule.rhs()
                        A = nonterminali[str(rule.lhs())]
                        B = nonterminali[str(bc[0])]
                        C = nonterminali[str(bc[1])]
                        if float(table[i][k][B]) > 0.0 and float(table[k][j][C]) > 0.0:
                            valcond = float(prob) * float(table[i][k][B]) * float(table[k][j][C])
                            if table[i][j][A] < valcond:
                                table[i][j][A] = valcond
                                back[i][j][A] = i, k, j, str(rule.lhs()), B, C

    maxval, root = max((table[0][N - 1][nonterminali[r]], r) for r in roots)

    if float(maxval) > 0.0:
        return buildTree(back, back[0][N - 1][nonterminali[root]])
    else:
        return '()'

# prende un albero in input e restiuisce le foglie dell'albero

def getPhrase(tree):
    sentence = []

    for subtree in tree.subtrees():
        if subtree.height() == 2:
            leaf = subtree.leaves()
            if leaf != []:
                sentence.append(leaf[0].lower())

    return sentence


# restituisce un albero dove le foglie contengono termini normalizzati

def morph(tree):

    for index, subtree in enumerate(tree):
        subtree.height()
        if subtree.height() > 2:
            lower(subtree)
        elif subtree.height() == 2:
            subtree[0] = get_morph(subtree[0])
            tree[index] = subtree

    return tree

# restitusce una parola normalizzata

def get_morph(word):
    word = word.lower()
    if word in morph:
        word = morph[word]
    return word

# restituisce un albero dove le foglie contengono termini in minuscolo

def lower(tree):
    for index, subtree in enumerate(tree):
        subtree.height()
        if subtree.height() > 2:
            lower(subtree)
        elif subtree.height() == 2:
            subtree[0] = subtree[0].lower()
            tree[index] = subtree

    return tree

# restituisce una stringa a partire dall'albero

def myTreeWriter(tree):

    nTree = ""
    splitTree = tree.pformat().encode('utf-8', 'ignore').split('\n')
    for i in xrange(0, len(splitTree)):
        sTree = splitTree[i].split(' ')
        for j in xrange(0, len(sTree)):
            if sTree[j] != '':
                nTree += str(sTree[j]) + ' '

    return nTree

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding("utf-8")
    load_morph()
    grammar, test = load_dataset()
    nonterminali = generateNonTerminal(grammar)
    load_morph()
    testFile = open("testV1.tst", "w")
    goldFile = open("goldV1.gld", "w")
    res = '()'
    for phrase in test:
        print phrase[0]
        res = probabilistcky(phrase[0], grammar, nonterminali)
        print res
        testFile.write(res + "\n")
        t = Tree.fromstring(phrase[1])
        t.collapse_unary(collapsePOS=True)
        t.chomsky_normal_form()
        t = morph(t)
        #t = lower(t)

        goldFile.write(myTreeWriter(t)+"\n")

    testFile.close()
    goldFile.close()

    os.system('EVALB/evalb goldV1.gld testV1.tst -e 109') #-e indica il numero di errori consentiti
