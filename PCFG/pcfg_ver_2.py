from nltk.tree import Tree
import nltk
import threading
import time
import random
import viterbi
import os
import baseline
hashRule = {}
grammarWords = {}
phraseDataSet = []

# carica morph-it

def load_morph():
    morph = {}
    in_file = "morph-it.txt"
    with open(in_file) as f:
        line_train = f.readlines()

    for i in line_train:
        line = i.split('\t')
        key = line[0]
        val = line[1]
        morph[key] = val

    return morph

# restitusce una parola normalizzata

def get_morph(word):
    word = word.lower()
    if word in morph:
        word = morph[word]
    return word

# restituisce un hashtable dove le chiavi sono i pos del treebank
# e i valori i pos di google

def transformationPos():


    pos = {}

    # AGGETTIVI
    pos['ADJ'] = 'ADJ'
    pos['ADJ~DE'] = 'ADJ'
    pos['ADJ~DI'] = 'ADJ'
    pos['ADJ~IN'] = 'ADJ'
    pos['ADJ~IR'] = 'ADJ'
    pos['ADJ~OR'] = 'ADJ'
    pos['ADJ~PO'] = 'ADJ'
    pos['ADJ~QU'] = 'ADJ'
    pos['ADJ~EX'] = 'ADJ'
    pos['PRDT'] = 'ADJ'
    pos['ADJP'] = 'ADJ'

    # AVVERBI
    pos['ADVB'] = 'ADV'

    #ARTICOLI
    pos['ART'] = 'DET'
    pos['ART~I'] = 'DET'
    pos['ART~D'] = 'DET'

    # VERBI
    pos['VMA'] = 'VERB'
    pos['VMO'] = 'VERB'
    pos['VMU'] = 'VERB'
    pos['VAU'] = 'VERB'
    pos['VCA'] = 'VERB'
    pos['RA'] = 'VERB'
    pos['RE'] = 'VERB'
    pos['FU'] = 'VERB'
    pos['CO'] = 'VERB'
    pos['CG'] = 'VERB'
    pos['GE'] = 'VERB'
    pos['IN'] = 'VERB'
    pos['IM'] = 'VERB'
    pos['IP'] = 'VERB'
    pos['PA'] = 'VERB'
    pos['PE'] = 'VERB'

    # COUNGIUNZIONI
    pos['CONJ'] = 'CONJ'

    # NOMI
    pos['NOU'] = 'NOUN'
    pos['NO~CP'] = 'NOUN'
    pos['NO~CA'] = 'NOUN'
    pos['NO~CS'] = 'NOUN'
    pos['NO~P'] = 'NOUN'

    # NUMERALI
    pos['NUM'] = 'NUM'
    pos['NUMR'] = 'NUM'

    # PRONOMI
    pos['PRO'] = 'PRON'
    pos['PRON'] = 'PRON'
    pos['PRON~DE'] = 'PRON'
    pos['PRON~ID'] = 'PRON'
    pos['PRON~IN'] = 'PRON'
    pos['PRON~LO'] = 'PRON'
    pos['PRON~OR'] = 'PRON'
    pos['PRON~PE'] = 'PRON'
    pos['PRON~PO'] = 'PRON'
    pos['PRON~RE'] = 'PRON'
    pos['PRON~RI'] = 'PRON'

    # PUNTEGGIATURA
    pos['.'] = '.'
    pos[','] = '.'
    pos[':'] = '.'
    pos[';'] = '.'

    # PREPOSIZIONI
    pos['PREP'] = 'ADP'

    # CONGIUNZIONI
    pos['CONJ'] = 'CONJ'

    # SCONOSCIUTO
    pos['-NONE-'] = 'X'
    pos['-LRB-'] = 'X'
    pos['-RRB-'] = 'X'
    pos['SPECIAL'] = 'X'
    pos['DATE'] = 'X'
    pos['PREDET'] = 'X'

    # PREDETERMINATORE
    pos['PUNCT'] = 'PRT'

    return pos

# restituisce una stringa a partire dall'albero

def myTreeWriter(tree):

    nTree = ""
    splitTree = tree.pformat().encode('utf-8', 'ignore').split('\n')
    for i in xrange(0, len(splitTree)):
        sTree = splitTree[i].split(' ')
        for j in xrange(0, len(sTree)):
            if sTree[j] != '':
                nTree += str(sTree[j]) + ' '

    return nTree

# sostituisce i terminali con i pos del treebank

def changePosAndRemoveWords(tree,pos):

    for index, subtree in enumerate(tree):
        subtree.height()
        if subtree.height() > 2:
            changePosAndRemoveWords(subtree,pos)
        elif subtree.height() == 2:
            lab = subtree.label()
            newVal = (pos[str(lab)])
            subtree[0] = str(newVal)
            tree[index] = subtree
    return tree



#sostituisce i terminali (pos di treebank) con i pos tag di google

def changeLeafPos(leaf, pos, couple):

    label = pos[str(leaf.label())]
    couple.addTuple((leaf[0], label))
    leaf[0] = label
    return leaf


# assegna un indice ad ogni non-terminale
# e lo memorizza in una hash table

def generateNonTerminal(grammar):
    nonterminali = {}

    count = 0
    prod = sorted(grammar.productions())
    for rule in prod:
        A = str(rule.lhs())
        if A not in nonterminali.keys():
            nonterminali[A] = count
            count += 1

    return nonterminali

# carica il dataset, count indica il numero
# di frasi che faranno parte del testset

def load_dataset():
    pos = transformationPos()
    in_file = "tut-clean-simple.penn"

    test = []
    prod = []
    with open(in_file) as f:
        line_train = f.readlines()
        #cont = 5
        cont = (len(line_train) * 10)/ 100
        for i in line_train:
            #i = changeTree(i)
            t = lower(Tree.fromstring(i))
            phrase = getPhrase(t)
            newTree = changePosAndRemoveWords(t,pos)
            if len(phrase) < 20 and cont > 0 and random.randint(0,1) == 1:
                test.append([phrase,myTreeWriter(newTree),i])
                cont -= 1
            else:
                t = newTree
                t.collapse_unary(collapsePOS=True)
                t.chomsky_normal_form()
                prod += t.productions()

    S = nltk.Nonterminal('S')
    grammar = nltk.induce_pcfg(S, prod)

    return grammar, test

# Costruisce la stringa risultante

def buildTree(back, val):
    if val[0] == -1:
        return '(' + val[1] + ' ' + val[2] + ')'
    s = '(' + val[3] + ' ' + buildTree(back, back[val[0]][val[1]][val[4]]) + ' ' + buildTree(back, back[val[1]][val[2]][
        val[5]]) + ' )'
    return s


# algoritmo principale

def probabilistcky(words, grammar, nonterminali):
    N = len(words) + 1
    table = [[[0.0 for a in nonterminali.keys()] for j in range(0, N)] for i in range(0, N)]
    back = [[[0.0 for a in nonterminali.keys()] for j in range(0, N)] for i in range(0, N)]

    roots = ['S', 'NP', 'PP', 'PRN']

    for j in range(1, N):
        for rule in sorted(grammar.productions(rhs=words[j - 1])):
            A = nonterminali[str(rule.lhs())]
            table[j - 1][j][A] = rule.prob()
            back[j - 1][j][A] = -1, str(rule.lhs()), words[j - 1]

        for i in reversed(range(-1, j - 1)):
            for k in range(i + 1, j + 1):
                for rule in grammar.productions():
                    if len(rule.rhs()) == 2:
                        prob = rule.prob()
                        bc = rule.rhs()
                        A = nonterminali[str(rule.lhs())]
                        B = nonterminali[str(bc[0])]
                        C = nonterminali[str(bc[1])]
                        if float(table[i][k][B]) > 0.0 and float(table[k][j][C]) > 0.0:
                            valcond = float(prob) * float(table[i][k][B]) * float(table[k][j][C])
                            if table[i][j][A] < valcond:
                                table[i][j][A] = valcond
                                back[i][j][A] = i, k, j, str(rule.lhs()), B, C


    maxval, root = max((table[0][N - 1][nonterminali[r]], r) for r in roots)
    if float(maxval) > 0.0:
        return buildTree(back, back[0][N - 1][nonterminali[root]])
    else:
        return '()'

# restituisce un albero dove le foglie contengono termini in minuscolo

def lower(tree):

    for index, subtree in enumerate(tree):
        subtree.height()
        if subtree.height() > 2:
            lower(subtree)
        elif subtree.height() == 2:
            subtree[0] = subtree[0].lower()
            tree[index] = subtree

    return tree

# prende un albero in input e restiuisce le foglie dell'albero

def getPhrase(tree):
    sentence = []

    for subtree in tree.subtrees():
        if subtree.height() == 2:
            leaf = subtree.leaves()
            if leaf != []:
                sentence.append(leaf[0])

    return sentence


if __name__ == '__main__':

    #morph = load_morph()
    grammar, test = load_dataset()
    nonterminali = generateNonTerminal(grammar)
    testFile = open("testV2.tst", "w")
    goldFile = open("goldV2.gld", "w")
    res = '()'
    viterbi.init({},False) #lower = False, no morph
    for phrase in test:
        tempPhrase = viterbi.viterbi(phrase[0])
        print phrase
        res = probabilistcky(tempPhrase, grammar, nonterminali)
        print res
        t1 = Tree.fromstring(phrase[1])
        t1.collapse_unary(collapsePOS=True)
        t1.chomsky_normal_form()
        #t1 = lower(t1)
        treeString = myTreeWriter(t1)
        print treeString
        #t = Tree.fromstring(phrase[1])
        #t.collapse_unary(collapsePOS=True)
        #t.chomsky_normal_form()

        testFile.write(res + "\n")
        goldFile.write(treeString+"\n")

    testFile.close()
    goldFile.close()

    os.system('EVALB/evalb goldV2.gld testV2.tst -e 109')
